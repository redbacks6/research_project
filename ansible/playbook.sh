#!/bash/sh

# ---------------------------------------------------------
# Nectar Playbook script
#
# Description:
#   This script executes all the necessary ansible playbook
#  scripts to configure servers and harvesters.
# ---------------------------------------------------------

INVENTORY="~/Developer/ResearchProject/ansible/ansible_hosts"
echo "[DEBUG] inventory=$INVENTORY"
SSH_PRIVATE_KEY="~/.ssh/nectar_rsa"
echo "[DEBUG] private key=$SSH_PRIVATE_KEY"

CMD="ansible-playbook -i $INVENTORY --private-key=$SSH_PRIVATE_KEY ~/Developer/ResearchProject/ansible/website_playbook.yml -vvvv"
echo "[DEBUG] website playbook command line=$CMD"
eval $CMD

# CMD="ansible-playbook -i $INVENTORY --private-key=$SSH_PRIVATE_KEY harvesters-setup.yml -vvvv"
# echo "[DEBUG] harvesters playbook command line=$CMD"
# eval $CMD
