#!/usr/bin/env python
"""
Twitter harvester - Streaming API
Author: Luke Jones
Email: lukealexanderjones@gmail.com/lukej1@student.unimelb.edu.au
Student ID: 654645
Date: 20 January 2016
"""

"""
Notes: I have stored my keys locally in a python script called credentials1 as I don't want to share on GitHub.
Just store the keys as a string with the correct names and import the file. 
consumer_key
consumer_secret
access_token_key
access_token_secret
Alternatively just drop them into the relevant fields.
"""

# External dependencies
import tweepy, couchdb, json, nltk, re
from textblob import TextBlob
import argparse
from pprint import pprint as pp
from interruptingcow import timeout
from nltk.corpus import stopwords
from nltk import word_tokenize

# Internal dependencies
import credentials1 as credentials #import your credentials here

# Setup the command line arguments
parser = argparse.ArgumentParser(description='Connect to Twitter Streaming API and store tweets in a CouchDB database')

parser.add_argument('-da', dest='database_address', type=str,
                   help='IP or DNS address of database including port number', 
                   default='http://127.0.0.1:5984', required=False)

parser.add_argument('-dr', dest='database_ref', type=str,
                   help='Name of the destination database', default='streaming')

parser.add_argument('query', type=str,
                   help='Search query for Streaming Connection')

parser.add_argument('-lang', dest='language', type=str,
                   help='Search language for Streaming Connection', default='en')

parser.add_argument('-loc', dest='locations', type=str,
                   help='SW - NE coordinates for search as E, N, E, N', default='')

# Global variables
args = parser.parse_args()
database_address = args.database_address
database_ref = args.database_ref
query = args.query
languages = args.language
locations = args.locations
stopwords = nltk.corpus.stopwords.words('english')

# Do things with the tweets here
def main():

	# Connections
	# https://pythonhosted.org/CouchDB/getting-started.html
	couch = couchdb.Server(database_address)
	couch.resource.credentials = (credentials.couch_username, 
																credentials.couch_password)
	#check if database created if Exception create it
	try:
		db = couch[database_ref]
	except Exception, e:
		db = couch.create(database_ref)

	#Use the Tweepy Package - docs here http://docs.tweepy.org/en/latest/index.html
	#set the keys
	auth = tweepy.OAuthHandler(credentials.consumer_key, 
														 credentials.consumer_secret)
	auth.set_access_token(credentials.access_token_key, 
												credentials.access_token_secret)
	# API constructor
	api = tweepy.API(auth, wait_on_rate_limit_notify=False)
	# Streaming API constructor
	sapi = tweepy.streaming.Stream(auth=auth, 
		  													 listener=CustomStreamListener(api, db))

	# Kill the stream every 10 minutes to stop the program running indefinitely
	# As stopping Supervisord does not kill the python script
	try:
		with timeout(60*10, exception=RuntimeError):
			# Filter based on queries
			sapi.filter(track=[query])
	except RuntimeError:
		pass

class CustomStreamListener(tweepy.StreamListener):
	#override tweepy.StreamListener to add logic to on_status
	def __init__(self, api, db):
		self.db = db
		self.api = api
		super(tweepy.StreamListener, self).__init__()

	# Do something with the tweets here
	def on_status(self, status):

		tweet = status._json

		if (status.lang == 'en') & (status.id_str not in self.db):
		
			# Sentiment https://github.com/sloria/textblob
			sentimentCont = TextBlob(tweet['text']).sentiment
			# This will override the default doc id in couchDB
			tweet['_id'] = status.id_str
			tweet['sentiment_polarity'] = sentimentCont[0]
			tweet['sentiment_subjectivity'] = sentimentCont[1]
			tweet['clean_text'] = clean_text(tweet['text'])

			# Tag the original tweet with the sentiment as well
			if 'retweeted_status' in tweet:
				tweet['retweeted_status']['sentiment_polarity'] = sentimentCont[0]
				tweet['retweeted_status']['sentiment_subjectivity'] = sentimentCont[1]
				tweet['retweeted_status']['clean_text'] = clean_text(tweet['retweeted_status']['text'])

			# Save the tweet
			self.db.save(tweet)

	def on_error(self, status_code):
		print >> sys.stderr, 'Encountered error with status code:', status_code
		return True # Don't kill the stream

	def on_timeout(self):
		print >> sys.stderr, 'Timeout...'
		return True # Don't kill the stream

def clean_text(document):
	#remove formatting and punctuation and non alpha numeric characters
	#fold to lower case
	cleaned = re.sub(r'[^a-z1-9 ]+', ' ', document.lower())
	#tokenise and remove stopwords
	final_list = [w for w in nltk.word_tokenize(cleaned) if w not in stopwords]

	return final_list

# Run the Main Method
if __name__ == '__main__':
    main()