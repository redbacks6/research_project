# Twitter Analytics Project

## Overview
This repository has been designed to sit alongside a Rails application has been designed to allow exploration of a twitter conversation stored on a couchdb database.  It allows a user to track a conversation or location and populate an existing CouchDB database with Tweets from Twitter.

### Table of Contents
**[Installation Instructions](#installation-instructions)**  
**[Usage Instructions](#usage-instructions)**  
**[Troubleshooting](#troubleshooting)**  

## Installation instructions
This repository isn't designed to be installed as such however a few instructions are provided to describe what the tweet harvester does and how I deployed it.

### Configuration
While I aimed to keep configuration to a minimum there is some minor configuration required to ensure the app works as expected.

### Database connection
The application requires a running [CouchDB](http://couchdb.apache.org) database.  The final app will use this [design document](https://bitbucket.org/redbacks6/couched-rails/src/master/db/design_doc.js), however this is not required for harvesting Tweets from Twitter.

### Tweet manipulation
The Tweets are slightly modified from those provided by Twitter when accessing via the Streaming API, namely they have been appended with the following tagged fields:

* Tagged words - I used a few basic packages from the [Natural Language Toolkit](http://www.nltk.org). Most basically this could be achieved with a .split(" ") type function.
    * clean_text - array containing tokenised words. This is used for word counts. Removal of stop words is recommended.
* Sentiment - I used [TextBlob](http://textblob.readthedocs.org/en/latest/) for tagged sentiment data
    * sentiment_polarity - a float from -1 (negative) to 1 (positive). This is used for sentiment analysis.
    * sentiment_subjectivity - a float from 0 (objective) to 1 (subjective). This is not used at the present time.

This can be done in any way you chose or you can use the [harvester script](https://bitbucket.org/redbacks6/research_project) which I prepared for this project.  Not that this is in a separate repository.

## Usage Instructions
The script can be run directly from the command line, however for best results (i.e. allowing the script to run in the background with automatic restarting, etc) it is best to run it using [Supervisor](http://supervisord.org)* or a similar process control system. The scripts required for this are located in [Scripts](https://bitbucket.org/redbacks6/research_project/src/master/scripts/) and should only require minor modification to start the harvester script.

All key variables can be changed without modifying the harvester script source code and are located in [harvester.sh](https://bitbucket.org/redbacks6/research_project/src/master/scripts/harvester.sh)

* SERVER - CouchDB database address
* DATABASE - Name of database to store the Tweets
* QUERY - Term you would like to track as per the [Streaming API Documentation](https://dev.twitter.com/streaming/overview/request-parameters)
* LANGUAGE - Language of Tweets returned
* LOCATION - Coordinates of bounding box as per the [Streaming API Documentation](https://dev.twitter.com/streaming/overview/request-parameters)

Lastly make sure that the address of the harvester script is correct.

*Note: that if you are not using Supervisor to manage process the python script will need to be modified slightly as its current implementation is hardcoded to shut down after 10mins (which is automatically restarted by Supervisor).  It has been implemented like as Supervisor stops the Bash script but not the Python script triggered by the Bash script.

### Dependencies
While I aimed to keep dependencies to a minimum some external software was used in this project. 
The harvester itself is a Python script, tested using Python 2.7. Furthermore dependancies are listed in the header of [twitter_harvester.py](https://bitbucket.org/redbacks6/research_project/src/master/twitter_harvester/twitter_harvester.py). Make sure that these packages are installed on the machine running the harvester.  These are all available via pip install.

## Troubleshooting
If an issue arises that isn't already addressed somewhere here then its probably best to contact me directly to discuss.